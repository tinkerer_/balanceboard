#include "testApp.h"

#define WINDOW_SCALE 14
//--------------------------------------------------------------
void testApp::setup(){
    serial.listDevices();
    ofEnableSmoothing();
    if(serial.setup("COM12",115200))
    {
        cout << "Serial setup OK"<< endl;
        serial.flush();
    }
    wii_bb.loadImage("Wii1024.png");
    wii_bb.resize(BB_WIDTH*WINDOW_SCALE*1.1, BB_HEIGHT*WINDOW_SCALE*1.2);
    ofSetFrameRate(50);

}

//--------------------------------------------------------------
void testApp::update(){
    uint16_t fl,fr,bl,br;
    int clears = 0;
    int available_bytes;
    unsigned char buf[100];
    uint8_t last_i = 0;
    ScreenCentre = ofPoint(ofGetWidth()/2,ofGetHeight()/2);
    available_bytes = serial.available();
    if(available_bytes > 80)
        serial.readBytes(buf,80);
    for(int  i=0;i<100;i++)
    {
        //static uint8_t serial_byte = buf[i];
        if(buf[i] == '\r' )
        {
            if(last_i != 0)
                sscanf((const char *)&buf[last_i]," %hu %hu %hu %hu",&sensor_values[0],&sensor_values[1],&sensor_values[2],&sensor_values[3]);
          //  serial_buffer.clear();
            clears++;
            last_i = i;
            //cout << fl << ' ' <<fr << ' ' <<bl << ' ' <<br << ' ' << endl;
        }
        //else
        //    serial_buffer.insert(serial_buffer.begin(),serial.readByte());

    }

}

//--------------------------------------------------------------
void testApp::draw(){
    float fl,fr,bl,br,x,y,grf;
    ofFill();
    ofBackground(ofColor::darkGray);
    ofSetColor(255);
    ofSetRectMode(OF_RECTMODE_CORNER);
    wii_bb.draw(ScreenCentre+(ofPoint(-wii_bb.width/2,-wii_bb.height/2)));
    ofSetRectMode(OF_RECTMODE_CORNER);

    fl = sensor_values[0]-sensor_offsets[0];
    fr = sensor_values[1]-sensor_offsets[1];
    bl = sensor_values[2]-sensor_offsets[2];
    br = sensor_values[3]-sensor_offsets[3];
    x = (fl+bl) - (fr+br);
    y = (fl+fr) - (br+bl);
    grf = fl+bl+fr+br;

    for(int i = 0; i<4;i++)
    {
        ofSetColor(ofColor::white);
        ofRect(20+(i*30),200,20,-sensor_values[i]*(200./65500));
        if(sensor_values[i] - sensor_offsets[i] < 0)
            ofSetColor(ofColor::red);
        ofRect(20+(i*30),400,20,-(sensor_values[i]-sensor_offsets[i])*(200./65500)*2);
    }
    //ofCircle(ofGetWidth()/2-(x/50),ofGetHeight()/2+(y/50),32);
    ofSetColor(ofColor::black);
    if(grf > 400)
        CoP = ofPoint((-x/grf)*(BB_WIDTH/2),(y/grf)*(BB_HEIGHT/2));
    else
        CoP = ofPoint(0,0);
    ofCircle(ScreenCentre+(CoP*WINDOW_SCALE),20);
    //ofCircle(ofGetWidth()/2-(x/50/(grf/100./75.)),ofGetHeight()/2+(y/50/(grf/100./75.)),30);
    ofDrawBitmapString(ofToString(grf/110.),ScreenCentre+ofPoint(0,30+(WINDOW_SCALE * BB_HEIGHT/2)));
    ofSetColor(ofColor::red);
    ofCircle(ofGetWidth()/2,ofGetHeight()/2,10);
    ofNoFill();
    ofSetLineWidth(4);
    ofSetRectMode(OF_RECTMODE_CENTER);
    ofRect(ScreenCentre,BB_WIDTH*WINDOW_SCALE,BB_HEIGHT*WINDOW_SCALE);
}


//--------------------------------------------------------------
void testApp::keyPressed(int key){
    switch(key)
    {
    case ' ':
        {
            for(int i =0; i< 4 ; i++)
                sensor_offsets[i] = sensor_values[i];
        }
    }

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}

void testApp::exit()
{
    serial.close();
}
